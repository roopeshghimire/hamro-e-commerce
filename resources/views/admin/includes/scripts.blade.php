<!-- Required Js -->
    <script src="{{ URL::asset('public/adminpanel/assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ URL::asset('public/adminpanel/assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/adminpanel/assets/js/plugins/feather.min.js') }}"></script>
    <script src="{{ URL::asset('public/adminpanel/assets/js/pcoded.min.js') }}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script> -->
    <!-- <script src="assets/js/plugins/clipboard.min.js"></script> -->
    <!-- <script src="assets/js/uikit.min.js"></script> -->

<!-- Apex Chart -->
<script src="{{ URL::asset('public/adminpanel/assets/js/plugins/apexcharts.min.js') }}"></script>
<script>
    $("body").append('<div class="fixed-button active"><a href="https://gumroad.com/dashboardkit" target="_blank" class="btn btn-md btn-success"><i class="material-icons-two-tone text-white">shopping_cart</i> Upgrade To Pro</a> </div>');
</script>

<!-- custom-chart js -->
<script src="{{ URL::asset('public/adminpanel/assets/js/pages/dashboard-sale.js') }}"></script>
</body>

</html>
